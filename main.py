import argparse
from random import randint

import pygame

from field import Field
from image_workers import RABBIT
from snake import Snake, SnakeDeadException


class RabbitManager:
    def __init__(self, field):
        self.field = field
        self.generate_new_rabbit()

    def generate_new_rabbit(self):
        free_ceils = []
        for row in self.field:
            for ceil in row:
                if not ceil.content:
                    free_ceils.append(ceil)
        target = randint(0, len(free_ceils) - 1)
        free_ceils[target].content = RABBIT


class SnakeGame:
    def __init__(self, height, width):
        pygame.init()
        self.field = Field(height, width)
        self.snake = Snake(self.field)
        self.rabbit_manager = RabbitManager(self.field)
        self.dead_rabbits = 0

    def on_execute(self):
        while True:
            self.snake.update_field()
            self.field.print()
            pygame.display.flip()

            pygame.time.delay(200)

            try:
                rabbits = self.snake.move_and_get_rabbits()
            except SnakeDeadException:
                self.field.print_score(self.dead_rabbits)
            else:
                if rabbits:
                    self.dead_rabbits += 1
                    self.rabbit_manager.generate_new_rabbit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Snake Game.')
    parser.add_argument('-H', type=int, help='Height of the field', default=15)
    parser.add_argument('-W', type=int, help='Width of the field', default=20)
    args = parser.parse_args()
    snake_game = SnakeGame(args.H, args.W)
    snake_game.on_execute()
