import pygame

from image_workers import GRASS, SIDE_SIZE


class FieldCell:
    def __init__(self):
        self.content = None

    def blit(self, screen, coords):
        screen.blit(GRASS, coords)
        if self.content:
            screen.blit(self.content, coords)


class Field:
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.field = [[FieldCell() for _ in range(width)] for _ in range(height)]
        self.screen = pygame.display.set_mode((SIDE_SIZE * width, SIDE_SIZE * height))

    def print(self):
        for y, row in enumerate(self.field):
            for x, field in enumerate(row):
                field.blit(self.screen, (SIDE_SIZE * x, SIDE_SIZE * y))

    def is_possible(self, y, x):
        return 0 <= y < self.height and 0 <= x < self.width

    def __getitem__(self, item):
        return self.field[item]

    def print_score(self, dead_rabbits):
        pygame.font.init()
        myfont = pygame.font.SysFont('Comic Sans MS', 30)
        textsurface = myfont.render(f'Your score: {dead_rabbits}', False, (255, 255, 255))
        self.screen.blit(textsurface, (0, 0))
        pygame.display.flip()
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()