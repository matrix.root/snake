import pygame

sprite = pygame.image.load('Snake.png')
SIDE_SIZE = 16

LEFT = (0, -1)
RIGHT = (0, +1)
UP = (-1, 0)
DOWN = (1, 0)


def get_part(x, y):
    part = pygame.Surface((SIDE_SIZE, SIDE_SIZE), pygame.SRCALPHA)
    part.blit(sprite, (0, 0), (x, y, x + SIDE_SIZE, y + SIDE_SIZE))
    return part


GRASS = get_part(48, 48)

RABBIT = get_part(32, 48)

DIRECTION_TO_HEAD = {
    UP: get_part(0, 0),
    RIGHT: get_part(16, 0),
    DOWN: get_part(32, 0),
    LEFT: get_part(48, 0),
}

DIRECTION_TO_TAIL = {
    UP: get_part(0, 16),
    RIGHT: get_part(16, 16),
    DOWN: get_part(32, 16),
    LEFT: get_part(48, 16),
}

B_UP = -1
B_DOWN = 1
B_LEFT = -2
B_RIGHT = 2

BORDERS_TO_BODY = {
    (B_UP, B_DOWN): get_part(0, 48),
    (B_LEFT, B_RIGHT): get_part(16, 48),
    (B_UP, B_RIGHT): get_part(32, 32),
    (B_DOWN, B_RIGHT): get_part(48, 32),
    (B_LEFT, B_DOWN): get_part(0, 32),
    (B_LEFT, B_UP): get_part(16, 32),
}
