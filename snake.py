from collections import deque

import pygame

from image_workers import UP, DIRECTION_TO_HEAD, DIRECTION_TO_TAIL, RABBIT, DOWN, B_UP, B_DOWN, LEFT, B_LEFT, \
    B_RIGHT, RIGHT, BORDERS_TO_BODY


class Snake:
    def __init__(self, field):
        self.body = deque(maxlen=field.height * field.width)
        x = field.width // 2
        y = field.height - 3
        for i in range(3):
            self.body.append(SnakeBody(y + i, x))
        self.field = field

    def update_field(self):
        self.body[0].print_head(self.field)
        for i in range(1, len(self.body) - 1):
            self.body[i].print_body(self.field, self.body[i - 1])
        self.body[-1].print_tail(self.field, self.body[-2])

    def move_and_get_rabbits(self):
        head = self.body[0]
        direction = self.get_direction(head.direction)
        y_delta, x_delta = direction
        target_y = head.y + y_delta
        target_x = head.x + x_delta
        if not self.field.is_possible(target_y, target_x):
            raise SnakeDeadException

        ceil = self.field[target_y][target_x]
        if not ceil.content:
            self.body.appendleft(SnakeBody(target_y, target_x, direction))
            tail = self.body.pop()
            tail.remove(self.field)
        elif ceil.content == RABBIT:
            self.body.appendleft(SnakeBody(target_y, target_x, direction))
            return 1
        else:
            raise SnakeDeadException

    @classmethod
    def get_direction(cls, current_direction):
        key_to_direction = {
            pygame.K_UP: UP,
            pygame.K_DOWN: DOWN,
            pygame.K_LEFT: LEFT,
            pygame.K_RIGHT: RIGHT,
        }
        direction = None
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            if event.type == pygame.KEYDOWN:
                new_direction = key_to_direction.get(event.key)
                if not cls.is_mirror(new_direction, current_direction):
                    direction = new_direction
        return direction or current_direction

    @staticmethod
    def is_mirror(first, second):
        x_1, y_1 = first
        x_2, y_2 = second
        is_mirror_by_y = x_1 == x_2 and y_1 == -y_2
        is_mirror_by_x = x_1 == -x_2 and y_1 == y_2
        return is_mirror_by_x or is_mirror_by_y


class SnakeBody:
    def __init__(self, y, x, direction=UP):
        self.y = y
        self.x = x
        self.direction = direction

    def get_ceil(self, field):
        return field[self.y][self.x]

    def print_head(self, field):
        self.get_ceil(field).content = DIRECTION_TO_HEAD[self.direction]

    def print_body(self, field, next_body):
        direction_to_border = {
            UP: B_UP,
            DOWN: B_DOWN,
            LEFT: B_LEFT,
            RIGHT: B_RIGHT,
        }
        borders = [
            direction_to_border[self.direction],
            -direction_to_border[next_body.direction],
        ]
        self.get_ceil(field).content = BORDERS_TO_BODY[tuple(sorted(borders))]

    def print_tail(self, field, prev):
        self.get_ceil(field).content = DIRECTION_TO_TAIL[prev.direction]

    def remove(self, field):
        self.get_ceil(field).content = None


class SnakeDeadException(Exception):
    def __str__(self):
        return 'Snake was dead'
